package com.example.tp1

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import kotlin.collections.List

interface OmdbApi {
    companion object{
        const val BASE_URL = "https://www.omdbapi.com"
        const val API_KEY = "b4d84aa"
    }

    @GET("/")
    fun searchMovie(
        @Query("s") title: String,
        @Query("apiKey") apiKey: String
    ) : Call<SearchResponse>

    @GET("/")
    fun getMovieById(
        @Query("i") IMDbID: String,
        @Query("apiKey") apiKey: String
    ) : Call<GetMovieResponse>
}
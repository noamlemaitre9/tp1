package com.example.tp1

import com.google.gson.annotations.SerializedName

data class ShortMovie(
    @SerializedName("Title") val Title: String,
    @SerializedName("imdbID") val imdbID: String,
    @SerializedName("Year") val Year: String,
    @SerializedName("Poster") val Poster: String
)
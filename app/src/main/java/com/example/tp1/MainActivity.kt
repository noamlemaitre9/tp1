package com.example.tp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        searchButton.setOnClickListener {
            val title :String = filmEditText.text.toString()

            if(title == "") {
                Toast.makeText(this, "Veuillez rentrer un titre !", Toast.LENGTH_SHORT).show()
            }
            else {

                val intent = Intent(this, MovieListActivity::class.java)
                intent.putExtra(MovieListActivity.EXTRA_TITLE, title)

                startActivity(intent)
            }
        }
    }
}

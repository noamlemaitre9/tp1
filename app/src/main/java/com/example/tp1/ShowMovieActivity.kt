package com.example.tp1

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_show_movie.*
import kotlinx.android.synthetic.main.item_movie.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ShowMovieActivity : AppCompatActivity(), Callback<GetMovieResponse> {

    companion object {
        const val EXTRA_FILM_ID = "ShowMovieActivity.EXTRA_FILM_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_show_movie)
        setSupportActionBar(toolbar)
    }

    override fun onResume() {
        super.onResume()

        getFilmById()
    }

    private fun getFilmById() {
        val id: String = intent.getStringExtra(EXTRA_FILM_ID)

        val retrofit = Retrofit.Builder()
            .baseUrl(OmdbApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(OmdbApi::class.java)
        api.getMovieById(id, OmdbApi.API_KEY).enqueue(this)
    }

    override fun onResponse(call: Call<GetMovieResponse>, response: Response<GetMovieResponse>) {

        response.body()?.let {
            Toast.makeText(
                this,
                "Film : " + it.Title,
                Toast.LENGTH_SHORT
            ).show()

            toolbar.title = it.Title
            Picasso.get().load(it.Poster).into(toolbarPoster)
        }

    }

    override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {
        Log.e("ShowMovieActivity", "Erreur lors de l'appel API", t)
        Toast.makeText(this, "Une erreur est survenue...", Toast.LENGTH_SHORT).show()
    }
}

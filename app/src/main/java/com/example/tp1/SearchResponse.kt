package com.example.tp1

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("Search") val movies: List<ShortMovie>,
    @SerializedName("totalResults") val totalResults: Int
    )
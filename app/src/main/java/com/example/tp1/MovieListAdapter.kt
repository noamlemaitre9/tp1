package com.example.tp1

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieListAdapter(private val context : Context) : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private val movies : ArrayList<ShortMovie> = ArrayList()

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(inflater.inflate(R.layout.item_movie, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int {
        return movies.size
    }



    fun setMovies(data : List<ShortMovie>) {
        movies.clear()
        movies.addAll(data)

        notifyDataSetChanged()
    }

    inner class ViewHolder(private val view : View) : RecyclerView.ViewHolder(view) {
        fun bind(shortMovie: ShortMovie) {
            Picasso.get().load(shortMovie.Poster).into(view.poster)
            view.title.text = shortMovie.Title

            view.setOnClickListener {
                val intent = Intent(context, ShowMovieActivity::class.java)
                intent.putExtra(ShowMovieActivity.EXTRA_FILM_ID, shortMovie.imdbID)

                startActivity(context, intent, null)
            }
        }

    }
}
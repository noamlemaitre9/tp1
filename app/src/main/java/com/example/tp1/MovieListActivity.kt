package com.example.tp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_movie_list.*
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

class MovieListActivity : AppCompatActivity(), Callback<SearchResponse> {

    companion object {
        const val EXTRA_TITLE = "MovieListActivity.EXTRA_TITLE"
    }

    private lateinit var adapter: MovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movie_list)

        adapter = MovieListAdapter(this)

        filmRecyclerView.layoutManager = LinearLayoutManager(this)
        filmRecyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        searchFilm()
    }

    private fun searchFilm() {
        val title: String = intent.getStringExtra(EXTRA_TITLE)

        val retrofit = Retrofit.Builder()
            .baseUrl(OmdbApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(OmdbApi::class.java)
        api.searchMovie(title, OmdbApi.API_KEY).enqueue(this)
    }

    override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {

        response.body()?.let {
            Toast.makeText(
                this,
                response.body()!!.totalResults.toString() + " résultats",
                Toast.LENGTH_SHORT
            ).show()

            adapter.setMovies(it.movies)
        }

    }

    override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
        Log.e("MovieListActivity", "Erreur lors de l'appel API", t)
        Toast.makeText(this, "Une erreur est survenue...", Toast.LENGTH_SHORT).show()
    }
}

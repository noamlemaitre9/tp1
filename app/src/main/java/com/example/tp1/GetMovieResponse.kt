package com.example.tp1

import com.google.gson.annotations.SerializedName

data class GetMovieResponse(
    @SerializedName("Title") val Title: String,
    @SerializedName("Year") val Year: String,
    @SerializedName("Runtime") val Runtime: String,
    @SerializedName("Genre") val Genre: String,
    @SerializedName("Poster") val Poster: String
)
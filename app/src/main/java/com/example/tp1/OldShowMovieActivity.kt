package com.example.tp1

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.old_activity_show_movie.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OldShowMovieActivity : AppCompatActivity(), Callback<GetMovieResponse> {

    companion object {
        const val EXTRA_FILM_ID = "ShowMovieActivity.EXTRA_FILM_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.old_activity_show_movie)
    }

    override fun onResume() {
        super.onResume()

        getFilmById()
    }

    private fun getFilmById() {
        val id: String = intent.getStringExtra(OldShowMovieActivity.EXTRA_FILM_ID)

        val retrofit = Retrofit.Builder()
            .baseUrl(OmdbApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(OmdbApi::class.java)
        api.getMovieById(id, OmdbApi.API_KEY).enqueue(this)
    }

    override fun onResponse(call: Call<GetMovieResponse>, response: Response<GetMovieResponse>) {

        response.body()?.let {
            Toast.makeText(
                this,
                "Film : " + response.body()!!.Title.toString(),
                Toast.LENGTH_SHORT
            ).show()

            // AFficher les infos du film
        }

    }

    override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {
        Log.e("ShowMovieActivity", "Erreur lors de l'appel API", t)
        Toast.makeText(this, "Une erreur est survenue...", Toast.LENGTH_SHORT).show()
    }

}
